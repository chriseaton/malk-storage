/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"testing"
)

// Tests the storage Driver struct and functionality.
func TestDriver(t *testing.T) {
	//Nothing for now
}
