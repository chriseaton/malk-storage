/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"strings"
	"testing"
	"time"
)

type TestModel struct {
	BaseModel
	Title       string
	Cats        int
	Furniture   *byte
	DateCreated time.Time
	ShortTerm   *GoalModel
	Favs        GoalModel
	unExThing   bool //this field shouldn't map (non-exported)
}

type GoalModel struct {
	BaseModel
	Goals     uint8
	Product   *ProductModel
	unExStuff *ProductModel //shouldn't map
}

type ProductModel struct {
	BaseModel
	Cost float32
	Qty  int16
}

// Tests the ModelValueMap.Items function to ensure the proper number and expected items are returned.
func TestModelValueMapItems(t *testing.T) {
	mm := make(ModelValueMap)
	mm["ID"] = ModelFieldValue{ModelField{reflect.TypeOf(uint64(0)), false, "ID"}, uint64(99)}
	mm["Name"] = ModelFieldValue{ModelField{reflect.TypeOf(""), false, "Name"}, "Mr. Jackson"}
	mm["Stars"] = ModelFieldValue{ModelField{reflect.TypeOf(int8(0)), false, "Stars"}, int8(9)}
	mm["Sub"] = ModelFieldValue{ModelField{reflect.TypeOf(TestModel{}), true, "Sub"}, TestModel{BaseModel{}, "Moose", 2, nil, time.Now(), nil, GoalModel{}, false}}
	mm["Sub.ID"] = ModelFieldValue{ModelField{reflect.TypeOf(uint64(0)), false, "ID"}, uint64(0)}
	mm["Sub.Title"] = ModelFieldValue{ModelField{reflect.TypeOf(""), false, "Title"}, "Moose"}
	mm["Sub.Cats"] = ModelFieldValue{ModelField{reflect.TypeOf(int(0)), false, "Cats"}, 42}
	//test Items func with no args
	i, err := mm.Items()
	assert.NoError(t, err)
	assert.Len(t, i, 7)
	//test Items func with specific args
	i, err = mm.Items("Name", "Sub.Cats", "ID")
	assert.NoError(t, err)
	if assert.Len(t, i, 3) {
		assert.Equal(t, "Mr. Jackson", i[0].Value)
		assert.Equal(t, 42, i[1].Value)
		assert.Equal(t, uint64(99), i[2].Value)
	}
	//test Items func with bad and good args
	i, err = mm.Items("Name", "DoesntExist")
	assert.Error(t, err)
	assert.Empty(t, i)
}

// Tests the Registry.Entities function to ensure a slice of all registered entity names are returned.
func TestRegistryEntities(t *testing.T) {
	r := &Registry{}
	r.entries = make(map[string]RegistryEntry)
	//add test entries
	r.entries["person"] = RegistryEntry{}
	r.entries["animal"] = RegistryEntry{}
	r.entries["vehicle"] = RegistryEntry{}
	//make check the function
	v := r.Entities()
	assert.Len(t, v, 3)
	assert.Contains(t, v, "person")
	assert.Contains(t, v, "animal")
	assert.Contains(t, v, "vehicle")
}

// Tests the Registry.Add function to ensure a new entry is created with an appropriate model map.
func TestRegistryAdd(t *testing.T) {
	r := &Registry{}
	//add the test entry
	err := r.Add("Test", func() Model { return &TestModel{} })
	assert.NoError(t, err)
	assert.Len(t, r.entries, 1)
	//check that the map was created properly
	entry, ok := r.entries["test"] //lowercase entity name (all entity names are stored this way in the entity map)
	if assert.True(t, ok) {
		assert.NotNil(t, entry.Type)
		assert.NotNil(t, entry.New)
		assert.NotEmpty(t, entry.Fields)
		assert.Len(t, entry.Fields, 19)
		//check name and type values in the generated map
		AssertModelField(t, "ID", reflect.TypeOf(uint64(0)), entry.Fields)
		AssertModelField(t, "Title", reflect.TypeOf(""), entry.Fields)
		AssertModelField(t, "Cats", reflect.TypeOf(0), entry.Fields)
		AssertModelField(t, "DateCreated", reflect.TypeOf(time.Time{}), entry.Fields)
		AssertModelField(t, "Furniture", reflect.TypeOf((*byte)(nil)), entry.Fields)
		AssertModelField(t, "ShortTerm", reflect.TypeOf((*GoalModel)(nil)), entry.Fields)
		AssertModelField(t, "ShortTerm.ID", reflect.TypeOf(uint64(0)), entry.Fields)
		AssertModelField(t, "ShortTerm.Goals", reflect.TypeOf(uint8(0)), entry.Fields)
		AssertModelField(t, "ShortTerm.Product", reflect.TypeOf((*ProductModel)(nil)), entry.Fields)
		AssertModelField(t, "ShortTerm.Product.ID", reflect.TypeOf(uint64(0)), entry.Fields)
		AssertModelField(t, "ShortTerm.Product.Cost", reflect.TypeOf(float32(0)), entry.Fields)
		AssertModelField(t, "ShortTerm.Product.Qty", reflect.TypeOf(int16(0)), entry.Fields)
		AssertModelField(t, "Favs", reflect.TypeOf(GoalModel{}), entry.Fields)
		AssertModelField(t, "Favs.ID", reflect.TypeOf(uint64(0)), entry.Fields)
		AssertModelField(t, "Favs.Goals", reflect.TypeOf(uint8(0)), entry.Fields)
		AssertModelField(t, "Favs.Product", reflect.TypeOf((*ProductModel)(nil)), entry.Fields)
		AssertModelField(t, "Favs.Product.ID", reflect.TypeOf(uint64(0)), entry.Fields)
		AssertModelField(t, "Favs.Product.Cost", reflect.TypeOf(float32(0)), entry.Fields)
		AssertModelField(t, "Favs.Product.Qty", reflect.TypeOf(int16(0)), entry.Fields)
	}
}

func AssertModelField(t *testing.T, path string, exType reflect.Type, fmap ModelMap) {
	entry, ok := fmap[path]
	if assert.True(t, ok) {
		name := path[strings.LastIndex(path, ".")+1:]
		assert.Equal(t, entry.Name, name)
		assert.Equal(t, entry.Type, exType)
	}
}

// Tests the Registry.Exists function to verify that an entity actually exists.
func TestRegistryExists(t *testing.T) {
	r := &Registry{}
	r.entries = make(map[string]RegistryEntry)
	//add test entries
	r.entries["person"] = RegistryEntry{}
	r.entries["animal"] = RegistryEntry{}
	r.entries["vehicle"] = RegistryEntry{}
	//test the exists function for an existing entity
	ok := r.Exists("Person")
	assert.True(t, ok)
	ok = r.Exists("PERSON")
	assert.True(t, ok)
	ok = r.Exists("PERson")
	assert.True(t, ok)
	//test the exists function for a non-existant entity
	ok = r.Exists("Invalid")
	assert.False(t, ok)
}

// Tests the Registry.Clear function and ensures all registry entries are cleared.
func TestRegistryClear(t *testing.T) {
	r := &Registry{}
	r.entries = make(map[string]RegistryEntry)
	//add test entries
	r.entries["person"] = RegistryEntry{}
	r.entries["animal"] = RegistryEntry{}
	r.entries["vehicle"] = RegistryEntry{}
	//test clear
	r.Clear()
	assert.Empty(t, r.entries)
}

// Tests the Registry.Remove function and ensures the specified entity is removed from the registry.
func TestRegistryRemove(t *testing.T) {
	r := &Registry{}
	r.entries = make(map[string]RegistryEntry)
	//add test entries
	r.entries["person"] = RegistryEntry{}
	r.entries["animal"] = RegistryEntry{}
	r.entries["vehicle"] = RegistryEntry{}
	//test remove existing
	err := r.Remove("AniMAL")
	assert.NoError(t, err)
	//test remove non-existing
	err = r.Remove("Animal")
	assert.Error(t, err)
}

// Tests the Registry.New function and ensures a proper model is created or not created.
func TestRegistryNew(t *testing.T) {
	r := &Registry{}
	r.entries = make(map[string]RegistryEntry)
	//add test entries
	r.entries["product"] = RegistryEntry{
		New: func() Model { return &ProductModel{} },
	}
	r.entries["nonew"] = RegistryEntry{}
	//test new on existing
	m := r.New("Product")
	assert.NotNil(t, m)
	//test new on non-existing entity
	m = r.New("Animal")
	assert.Nil(t, m)
	//test existing entity with no New definition
	m = r.New("NoNew")
	assert.Nil(t, m)
}

// Tests the Registry.FieldValueMap function to ensure it maps values properly.
func TestRegistryFieldMaps(t *testing.T) {
	r := &Registry{}
	//add the test entry
	err := r.Add("Test", func() Model { return &TestModel{} })
	ok := assert.NoError(t, err)
	if ok && assert.Len(t, r.entries, 1) {
		//get the field map
		fm, err := r.FieldMap("Test")
		assert.NoError(t, err)
		assert.NotEmpty(t, fm)
		assert.Len(t, fm, 19)
		//create test model
		furn := byte(55)
		m := &TestModel{
			Title:     "Finn t. Human",
			Cats:      89,
			Furniture: &furn,
			ShortTerm: &GoalModel{Goals: uint8(188)},
			Favs: GoalModel{
				Goals: uint8(10),
				Product: &ProductModel{
					Cost: float32(42.44),
					Qty:  int16(1202),
				},
			},
		}
		//test map the values
		fvm, err := r.FieldValueMap("Test", m)
		assert.NoError(t, err)
		assert.NotEmpty(t, fvm)
	}
}
