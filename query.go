/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

const (
	ErrorTypeMismatch             = "Type mismatch, type kind %q is not equal to type kind %q."
	ErrorOperatorInvalid          = "The comparison operator %q is invalid."
	ErrorKindOperatorNotSupported = "Value kind %q is not supported for comparison operator %q."
	ErrorNilOperatorNotSupported  = "A nil value cannot be compared with operator %q."
)

const (
	QueryCompareEqual              = "=="
	QueryCompareNotEqual           = "!="
	QueryCompareGreaterThan        = ">"
	QueryCompareGreaterThanEqualTo = ">="
	QueryCompareLessThan           = "<"
	QueryCompareLessThanEqualTo    = "<="
	QueryCompareLike               = "LIKE"
	QueryCompareNotLike            = "NOT LIKE"
)

// Stores the values of requested fields by model ID (uint64) and field name (string).
type QueryFieldResultMap map[uint64]map[string]interface{}

// Represents a query to the storage driver that can contain logical AND/OR conditions.
type Query struct {

	// The entity to query against.
	Entity string

	// Slice of conditions that must be met for a model to be included in the result set.
	RootCondition *QueryCondition
}

// Creates new QueryCondition and sets it as this query's root condition.
// The condition is returned, and can be chained with logical calls to And() and Or().
func (q *Query) Condition(field string, comparison string, value interface{}) *QueryCondition {
	q.RootCondition = &QueryCondition{
		FieldPath:  field,
		Comparison: comparison,
		Value:      value,
	}
	return q.RootCondition
}

// Clears all conditions from the query.
func (q *Query) Reset() {
	q.RootCondition = nil
}

// Validates the root condition and all sub-conditions for errors. Storage drivers should generally call this before
// the execution and parsing of every Query call.
func (q *Query) Validate() error {
	if q.RootCondition != nil {
		err := q.RootCondition.validate(true)
		if err != nil {
			return err
		}
	}
	return nil
}

// Evaluates the query against a model's value map.
// If no conditions are specified, the evaluation returns true.
func (q *Query) Eval(vmap ModelValueMap) (bool, error) {
	if q.RootCondition != nil {
		return q.RootCondition.Eval(vmap)
	}
	return true, nil
}

// Represents a logical condition for use with the storage driver. The FieldName, Comparison and Values must all be
// non-nil.
type QueryCondition struct {
	FieldPath  string
	Comparison string
	Value      interface{}
	Logic      string
	Next       []*QueryCondition
}

// Converts the condition to a readable string representing the logic of the condition.
func (c *QueryCondition) String() string {
	output := fmt.Sprintf(" %s %s", c.FieldPath, c.Comparison)
	if _, ok := c.Value.(string); ok {
		output += fmt.Sprintf(" %q", c.Value.(string))
	} else {
		output += fmt.Sprintf(" %v", c.Value)
	}
	for _, v := range c.Next {
		output += fmt.Sprintf(" %s ", strings.ToUpper(v.Logic))
		if len(v.Next) > 0 {
			output += "("
		}
		output += v.String()
		if len(v.Next) > 0 {
			output += " )"
		}
	}
	return strings.TrimLeft(output, " ")
}

// Appends a logical AND condition to the current condition then returns the current condition (not the newly
// added condition).
func (c *QueryCondition) And(field string, comparison string, value interface{}) *QueryCondition {
	c.appendCondition(field, comparison, value, "and")
	return c
}

// Appends a logical OR condition to the current condition then returns the current condition (not the newly
// added condition).
func (c *QueryCondition) Or(field string, comparison string, value interface{}) *QueryCondition {
	c.appendCondition(field, comparison, value, "or")
	return c
}

// Appends a logical AND condition to the current condition and returns the newly created condition (chained calls will
// then apply to the newly-created condition).
func (c *QueryCondition) GroupAnd(field string, comparison string, value interface{}) *QueryCondition {
	return c.appendCondition(field, comparison, value, "and")
}

// Appends a logical OR condition to the current condition and returns the newly created condition (chained calls will
// then apply to the newly-created condition).
func (c *QueryCondition) GroupOr(field string, comparison string, value interface{}) *QueryCondition {
	return c.appendCondition(field, comparison, value, "or")
}

// Appends a local condition to the current condition and returns the newly created condition
func (c *QueryCondition) appendCondition(field string, comparison string, value interface{}, logic string) *QueryCondition {
	n := &QueryCondition{
		FieldPath:  field,
		Comparison: comparison,
		Value:      value,
		Logic:      logic,
	}
	c.Next = append(c.Next, n)
	return n
}

// checks to make sure the condition has valid values. If an issue is detected, it is returned as an error.
func (c *QueryCondition) validate(isRoot bool) error {
	l := strings.ToUpper(c.Logic)
	comp := strings.ToUpper(c.Comparison)
	if (isRoot && l != "" || isRoot == false) && l != "AND" && l != "OR" {
		return fmt.Errorf("Invalid logic value. Must be %q or %q", "and", "or")
	} else if c.FieldPath == "" {
		return fmt.Errorf("The \"FieldName\" field must be specified.")
	} else if c.Comparison == "" {
		return fmt.Errorf("The \"Comparison\" field must be specified.")
	} else if comp != QueryCompareEqual &&
		comp != QueryCompareNotEqual &&
		comp != QueryCompareGreaterThan &&
		comp != QueryCompareGreaterThanEqualTo &&
		comp != QueryCompareLessThan &&
		comp != QueryCompareLessThanEqualTo &&
		comp != QueryCompareLike &&
		comp != QueryCompareNotLike {
		return fmt.Errorf(ErrorOperatorInvalid, comp)
	} else if _, ok := c.Value.(string); comp == QueryCompareLike && ok == false {
		return fmt.Errorf("The \"LIKE\" comparison can only be used on string values.")
	} else if _, ok := c.Value.(string); comp == QueryCompareNotLike && ok == false {
		return fmt.Errorf("The \"NOT LIKE\" comparison can only be used on string values.")
	}
	//ensure value is a solid type
	t := reflect.TypeOf(c.Value)
	if isModelFieldType(t) == false {
		return fmt.Errorf(ErrorModelFieldTypeNotSupported, t.Name())
	}
	//validate next items
	for _, c := range c.Next {
		if err := c.validate(false); err != nil {
			return err
		}
	}
	return nil
}

// Evaluates the QueryCondition agains a map of values.
func (c *QueryCondition) Eval(vmap ModelValueMap) (bool, error) {
	//make proper comparison to condition value
	m, ok := vmap[c.FieldPath]
	if ok {
		final, err := c.compare(m.Value)
		if err != nil {
			return false, err
		} else {
			//cycle next conditions, building upon final result
			for _, nc := range c.Next {
				l := strings.ToUpper(nc.Logic)
				result, err := nc.Eval(vmap)
				if err != nil {
					return false, err
				} else {
					if l == "AND" {
						final = final && result
					} else {
						final = final || result
					}
				}
			}
		}
		return final, nil
	}
	return false, fmt.Errorf("Unable to locate condition field path %q in the provided value map.", c.FieldPath)
}

// Compares the given value with the condition's specified comparison and value (only this condition).
// Comparisons are strictly typed, regardless if automatic conversions by Go are allowed, this means if you want to
// compare against an int64 type, you must pass an int64 input, otherwise you will get a type mismatch error.
func (c *QueryCondition) compare(v interface{}) (bool, error) {
	t := c.Value
	//handle nil values (not supported)
	if c.Value == nil || v == nil {
		return false, fmt.Errorf(ErrorNilOperatorNotSupported, c.Comparison)
	}
	//handle empty condition
	if c.FieldPath == "" && c.Comparison == "" {
		return true, nil
	}
	//get type information
	vt := reflect.TypeOf(v)
	tt := reflect.TypeOf(t)
	//compare
	var final bool
	var err error
	if c.Comparison == QueryCompareEqual {
		final, err = c.compareEqual(v, vt, t, tt)
	} else if c.Comparison == QueryCompareNotEqual {
		final, err = c.compareNotEqual(v, vt, t, tt)
	} else if c.Comparison == QueryCompareGreaterThan {
		final, err = c.compareGreaterThan(v, vt, t, tt)
	} else if c.Comparison == QueryCompareGreaterThanEqualTo {
		final, err = c.compareGreaterThanEqualTo(v, vt, t, tt)
	} else if c.Comparison == QueryCompareLessThan {
		final, err = c.compareLessThan(v, vt, t, tt)
	} else if c.Comparison == QueryCompareLessThanEqualTo {
		final, err = c.compareLessThanEqualTo(v, vt, t, tt)
	} else if c.Comparison == QueryCompareLike {
		final, err = c.compareLike(v, vt, t, tt)
	} else if c.Comparison == QueryCompareNotLike {
		final, err = c.compareNotLike(v, vt, t, tt)
	} else {
		return final, fmt.Errorf(ErrorOperatorInvalid, c.Comparison)
	}
	return final, err
}

// Checks if v and t have the same value.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareEqual(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	} else if v == t {
		return true, nil
	}
	return false, nil
}

// Checks if v and t have unequal values.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareNotEqual(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	} else if v != t {
		return true, nil
	}
	return false, nil
}

// Checks if v is greater than t.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareGreaterThan(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	}
	vk := vt.Kind()
	switch vk {
	case reflect.Bool:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, "bool", ">")
	case reflect.String:
		return (v.(string) > t.(string)), nil
	case reflect.Int:
		return (v.(int) > t.(int)), nil
	case reflect.Int8:
		return (v.(int8) > t.(int8)), nil
	case reflect.Int16:
		return (v.(int16) > t.(int16)), nil
	case reflect.Int32:
		return (v.(int32) > t.(int32)), nil
	case reflect.Int64:
		return (v.(int64) > t.(int64)), nil
	case reflect.Uint:
		return (v.(uint) > t.(uint)), nil
	case reflect.Uint8:
		return (v.(uint8) > t.(uint8)), nil
	case reflect.Uint16:
		return (v.(uint16) > t.(uint16)), nil
	case reflect.Uint32:
		return (v.(uint32) > t.(uint32)), nil
	case reflect.Uint64:
		return (v.(uint64) > t.(uint64)), nil
	case reflect.Float32:
		return (v.(float32) > t.(float32)), nil
	case reflect.Float64:
		return (v.(float64) > t.(float64)), nil
	default:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), ">")
	}
}

// Checks if v is greater than or equal to t.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareGreaterThanEqualTo(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	}
	vk := vt.Kind()
	switch vk {
	case reflect.Bool:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, "bool", ">=")
	case reflect.String:
		return (v.(string) >= t.(string)), nil
	case reflect.Int:
		return (v.(int) >= t.(int)), nil
	case reflect.Int8:
		return (v.(int8) >= t.(int8)), nil
	case reflect.Int16:
		return (v.(int16) >= t.(int16)), nil
	case reflect.Int32:
		return (v.(int32) >= t.(int32)), nil
	case reflect.Int64:
		return (v.(int64) >= t.(int64)), nil
	case reflect.Uint:
		return (v.(uint) >= t.(uint)), nil
	case reflect.Uint8:
		return (v.(uint8) >= t.(uint8)), nil
	case reflect.Uint16:
		return (v.(uint16) >= t.(uint16)), nil
	case reflect.Uint32:
		return (v.(uint32) >= t.(uint32)), nil
	case reflect.Uint64:
		return (v.(uint64) >= t.(uint64)), nil
	case reflect.Float32:
		return (v.(float32) >= t.(float32)), nil
	case reflect.Float64:
		return (v.(float64) >= t.(float64)), nil
	default:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), ">=")
	}
}

// Checks if v is less than t.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareLessThan(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	}
	vk := vt.Kind()
	switch vk {
	case reflect.Bool:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, "bool", "<")
	case reflect.String:
		return (v.(string) < t.(string)), nil
	case reflect.Int:
		return (v.(int) < t.(int)), nil
	case reflect.Int8:
		return (v.(int8) < t.(int8)), nil
	case reflect.Int16:
		return (v.(int16) < t.(int16)), nil
	case reflect.Int32:
		return (v.(int32) < t.(int32)), nil
	case reflect.Int64:
		return (v.(int64) < t.(int64)), nil
	case reflect.Uint:
		return (v.(uint) < t.(uint)), nil
	case reflect.Uint8:
		return (v.(uint8) < t.(uint8)), nil
	case reflect.Uint16:
		return (v.(uint16) < t.(uint16)), nil
	case reflect.Uint32:
		return (v.(uint32) < t.(uint32)), nil
	case reflect.Uint64:
		return (v.(uint64) < t.(uint64)), nil
	case reflect.Float32:
		return (v.(float32) < t.(float32)), nil
	case reflect.Float64:
		return (v.(float64) < t.(float64)), nil
	default:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), "<")
	}
}

// Checks if v is less than or equal to t.
// An error is returned if vt and tt are not the same reflect.Type.
func (c *QueryCondition) compareLessThanEqualTo(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	}
	vk := vt.Kind()
	switch vk {
	case reflect.Bool:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, "bool", "<=")
	case reflect.String:
		return (v.(string) <= t.(string)), nil
	case reflect.Int:
		return (v.(int) <= t.(int)), nil
	case reflect.Int8:
		return (v.(int8) <= t.(int8)), nil
	case reflect.Int16:
		return (v.(int16) <= t.(int16)), nil
	case reflect.Int32:
		return (v.(int32) <= t.(int32)), nil
	case reflect.Int64:
		return (v.(int64) <= t.(int64)), nil
	case reflect.Uint:
		return (v.(uint) <= t.(uint)), nil
	case reflect.Uint8:
		return (v.(uint8) <= t.(uint8)), nil
	case reflect.Uint16:
		return (v.(uint16) <= t.(uint16)), nil
	case reflect.Uint32:
		return (v.(uint32) <= t.(uint32)), nil
	case reflect.Uint64:
		return (v.(uint64) <= t.(uint64)), nil
	case reflect.Float32:
		return (v.(float32) <= t.(float32)), nil
	case reflect.Float64:
		return (v.(float64) <= t.(float64)), nil
	default:
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), "<=")
	}
}

// Checks if v is like t using a string comparison.
// An error is returned if vt and tt are not the same reflect.Type or if they are not of the String type kind.
func (c *QueryCondition) compareLike(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	} else if vt.Kind() != reflect.String {
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), QueryCompareLike)
	}
	like := "^" + strings.Replace(regexp.QuoteMeta(t.(string)), "\\*", ".*", -1) + "$"
	return regexp.MatchString(like, v.(string))
}

// Checks if v is not like t using a string comparison.
// An error is returned if vt and tt are not the same reflect.Type or if they are not of the String type kind.
func (c *QueryCondition) compareNotLike(v interface{}, vt reflect.Type, t interface{}, tt reflect.Type) (bool, error) {
	if vt != tt {
		return false, fmt.Errorf(ErrorTypeMismatch, vt.Name(), tt.Name())
	} else if vt.Kind() != reflect.String {
		return false, fmt.Errorf(ErrorKindOperatorNotSupported, vt.Kind(), QueryCompareNotLike)
	}
	like := "^" + strings.Replace(regexp.QuoteMeta(t.(string)), "\\*", ".*", -1) + "$"
	m, err := regexp.MatchString(like, v.(string))
	return !m, err
}
