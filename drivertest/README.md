![malk storage](https://gitlab.com/uploads/project/avatar/1059093/malk-storage.png "malk storage")
# [malk-storage](https://gitlab.com/chriseaton/malk-storage) / [drivertest](https://gitlab.com/chriseaton/malk-storage/drivertest)

# Malk Storage Driver Test Package
This package was created to help facilitate testing of custom Malk storage drivers. It works by sequentially calling
storage.Driver interface functions on your custom storage driver. It first creates a set of test models with
randomized values then performs various retrieval and modifying actions on them.

**Getting started** is easy, and for nearly all purposes, all you need to do is call the `drivertest.Run` function,
as shown here:
````go
import "gitlab.com/chriseaton/malk-storage/drivertest"

func TestDriverInterfaceCalls(t *testing.T) {
	//create your driver
	d := &MyCustomDriver{}
	//run full interface test (Save, Get, Query, Delete, etc.)
	drivertest.Run(d, t)
}
````
It is *recommended* to utilize the above method for testing, as it will call Configure on your driver with a
pre-configured registry object, then run the tests in sequence, first creating test model entries, then working with
them.

## Package Functions
You can call the individual test functions that are called sequentially by `drivertest.Run` if you so desire.
If you wish to create your own copies of new, randomized-value test models, you can call the
`drivertest.CreatePrimaryModel` or `drivertest.CreateSecondaryModel` functions respectively.

*Note these function generally rely on the `storage.DriverTestSave` function creating models with IDs 1-20. If you
are running any of these out of order, make sure your storage medium has models stored with those IDs.

| Function | Parameters | Description |
| -------- | ---------- | ----------- |
| `CreateSecondaryModel` | `*rand.Rand` *returns `PrimaryModel`* | Creates a primary model (used only for testing) with random values for all fields. |
| `CreateSecondaryModel` | `*rand.Rand` *returns `SecondaryModel`* | Creates a secondary model with random values for all fields. The secondary model is not registered with an entity, but is used as a sub-struct on the primary model. |

### Test Functions
Each test function will return a boolean indicating whether the test was successful or not.

| Function | Parameters | Description |
| -------- | ---------- | ----------- |
| `DriverTestSave` | `storage.Driver, *testing.T` *returns `bool`* | Creates test models with randomized values, then stores them through the driver's Save and SaveAll functions. All save functions and results are tested. |
| `DriverTestGet` |  `storage.Driver, *testing.T` *returns `bool`* | Tests the driver's ability to retrieve models through the "Get" and "GetAll" functions. All get functions and results are tested. |
| `DriverTestExists` | `storage.Driver, *testing.T` *returns `bool`* | Tests the driver's function to check for existing models through the "Exists" function. The exist call and it's results are tested. |
| `DriverTestQuery` | `storage.Driver, *testing.T` *returns `bool`* | Tests the driver's function to query models through the "Query" and "QueryFields" functions. All query functions and results are tested. <br />This function requires models that contain pre-defined values, so it is not recommended to call this manually. |
| `DriverTestDelete` | `storage.Driver, *testing.T` *returns `bool`* | Tests the driver's function to delete models through the "Delete" and "DeleteAll" functions. All delete functions and results are tested. |
| `DriverTestTruncate` | `storage.Driver, *testing.T` *returns `bool`* | Tests the driver's function to query models through the "Truncate" function. The truncate function and it's results are tested.
| `DriverTestCycle` | `storage.Driver, *testing.T` *returns `bool`* | Final test that validates a saved model's field values. After the model is saved, it should be retrieved with the exact same values from Get* and Query* methods.