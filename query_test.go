/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

// Tests the storage Query condition-chaining.
func TestQueryConditionChaining(t *testing.T) {
	//ensure the root condition is created
	q := &Query{Entity: "people"}
	rc := q.Condition("name", "!=", "John")
	assert.NotNil(t, rc)
	assert.Equal(t, q.RootCondition, rc, "Query condition did not return the root condition.")
	//test chaining
	rc.And("stars", ">=", 5).And("stars", "<=", 10)
	assert.Equal(t, "name != \"John\" AND stars >= 5 AND stars <= 10", rc.String(), "Query conditions produced an unmatching string representation.")
	rc.GroupAnd("name", "like", "smit*").Or("name", "like", "jan*")
	assert.Equal(t, "name != \"John\" AND stars >= 5 AND stars <= 10 AND (name like \"smit*\" OR name like \"jan*\" )", rc.String(), "Query conditions produced an unmatching string representation.")
	//test reset
	q.Reset()
	assert.Nil(t, q.RootCondition)
}

// Tests the storage Query validation functionality.
func TestQueryValidation(t *testing.T) {
	q := &Query{Entity: "people"}
	q.Condition("name", "!=", "John").And("stars", ">=", 5).And("stars", "<=", 10)
	err := q.Validate()
	assert.NoError(t, err)
	//test large chain validation
	q = &Query{Entity: "people"}
	q.Condition("name", "!=", "John").
		And("stars", ">=", 5).
		And("stars", "<=", 10).
		GroupAnd("name", "like", "smit*").
		Or("name", "like", "jan*")
	err = q.Validate()
	assert.NoError(t, err)
}

// Checks that query conditions evaluate properly.
func TestQueryEvaluation(t *testing.T) {
	vmap := make(ModelValueMap)
	vmap["Stars"] = ModelFieldValue{
		ModelField: ModelField{Type: reflect.TypeOf(int8(0))},
		Value:      int8(33),
	}
	vmap["Something"] = ModelFieldValue{
		ModelField: ModelField{Type: reflect.TypeOf(int8(0))},
		Value:      int8(88),
	}
	qc := &QueryCondition{
		FieldPath:  "Stars",
		Comparison: "<=",
		Value:      int8(2),
	}
	qc.Or("Stars", ">", int8(91)).
		GroupOr("Stars", "==", int8(23)).
		Or("Stars", "==", int8(33))
	matched, err := qc.Eval(vmap)
	assert.NoError(t, err)
	assert.True(t, matched, "Evaluation of vmap against condition should have matched, but didn't.")
}

// Checks that query conditions evaluate properly.
func TestQueryConditions(t *testing.T) {
	boolPtr1 := true
	boolPtr2 := false
	stringPtr1 := "hello"
	stringPtr2 := "world"
	t1 := time.Date(2000, 12, 2, 8, 12, 44, 4222, time.UTC)
	t3 := time.Date(2000, 12, 2, 8, 12, 44, 4222, time.UTC)
	t2 := time.Now().Add(time.Duration(-48) * time.Hour)
	tests := map[string][][]interface{}{
		//Equality comparisons
		QueryCompareEqual: [][]interface{}{
			//layout = {value}, {equal value}, {unequal value}, {invalid value}
			[]interface{}{true, true, false, int8(4)},
			[]interface{}{true, true, false, nil},
			[]interface{}{&boolPtr1, &boolPtr1, &boolPtr2, nil},
			[]interface{}{int8(33), int8(33), int8(4), true},
			[]interface{}{int16(33), int16(33), int16(4), int32(33)},
			[]interface{}{int32(33), int32(33), int32(4), uint32(33)},
			[]interface{}{int64(33), int64(33), int64(4), true},
			[]interface{}{uint8(33), uint8(33), uint8(4), int32(33)},
			[]interface{}{uint16(33), uint16(33), uint16(4), true},
			[]interface{}{uint32(33), uint32(33), uint32(4), '+'},
			[]interface{}{uint64(33), uint64(33), uint64(4), true},
			[]interface{}{float32(33), float32(33), float32(4), true},
			[]interface{}{float64(33), float64(33), float64(4), true},
			[]interface{}{"hello", "hello", "goodbye", 28945},
			[]interface{}{"28945", "28945", "348777", 28945},
			[]interface{}{&stringPtr1, &stringPtr1, &stringPtr2, 28945},
			[]interface{}{t1, t3, t2, nil},
			[]interface{}{&t1, &t1, &t2, 456454564},
		},
		//Inequality comparisons
		QueryCompareNotEqual: [][]interface{}{
			//layout = {value}, {unequal value}, {equal value}, {invalid value}
			[]interface{}{true, false, true, int8(4)},
			[]interface{}{true, false, true, nil},
			[]interface{}{&boolPtr1, &boolPtr2, &boolPtr1, nil},
			[]interface{}{int8(33), int8(8), int8(33), true},
			[]interface{}{int16(33), int16(346), int16(33), int32(33)},
			[]interface{}{int32(33), int32(7), int32(33), uint32(33)},
			[]interface{}{int64(33), int64(53), int64(33), true},
			[]interface{}{uint8(33), uint8(26), uint8(33), int32(33)},
			[]interface{}{uint16(33), uint16(223), uint16(33), true},
			[]interface{}{uint32(33), uint32(345), uint32(33), true},
			[]interface{}{uint64(33), uint64(535), uint64(33), true},
			[]interface{}{float32(33), float32(34), float32(33), true},
			[]interface{}{float64(33), float64(44), float64(33), true},
			[]interface{}{"hello", "goodbye", "hello", 28945},
			[]interface{}{"28945", "348777", "28945", 28945},
			[]interface{}{&stringPtr1, &stringPtr2, &stringPtr1, 28945},
			[]interface{}{t1, t2, t1, nil},
			[]interface{}{&t1, &t2, &t1, 456454564},
		},
		//Greater than comparisons
		QueryCompareGreaterThan: [][]interface{}{
			//layout = {value}, {greater value}, {equal or less value}, {invalid value}
			[]interface{}{int8(5), int8(32), int8(1), uint8(4)},
			[]interface{}{int16(5), int16(32), int16(1), uint8(4)},
			[]interface{}{int32(5), int32(32), int32(1), "4"},
			[]interface{}{int64(5), int64(32), int64(5), uint8(4)},
			[]interface{}{uint8(5), uint8(32), uint8(1), true},
			[]interface{}{uint16(5), uint16(32), uint16(1), false},
			[]interface{}{uint32(5), uint32(32), uint32(5), uint8(4)},
			[]interface{}{uint64(5), uint64(32), uint64(1), "test"},
			[]interface{}{float32(5), float32(32), float32(5), true},
			[]interface{}{float64(5), float64(32), float64(1), uint8(4)},
			[]interface{}{"hello", "hellooo", "hello", 28945},
			[]interface{}{"28945", "348777", "28945", 28945},
			[]interface{}{"abc", "bcd", "abc", false},
		},
		//Greater than or equal to comparisons
		QueryCompareGreaterThanEqualTo: [][]interface{}{
			//layout = {value}, {greater or equal value}, {less value}, {invalid value}
			[]interface{}{int8(5), int8(32), int8(1), uint8(4)},
			[]interface{}{int16(5), int16(5), int16(1), uint8(4)},
			[]interface{}{int32(5), int32(32), int32(1), "4"},
			[]interface{}{int64(5), int64(32), int64(4), uint8(4)},
			[]interface{}{uint8(5), uint8(5), uint8(1), true},
			[]interface{}{uint16(5), uint16(5), uint16(1), false},
			[]interface{}{uint32(5), uint32(32), uint32(4), uint8(4)},
			[]interface{}{uint64(5), uint64(32), uint64(1), uint8(4)},
			[]interface{}{float32(5), float32(5), float32(4), true},
			[]interface{}{float64(5), float64(32), float64(1), uint8(4)},
			[]interface{}{"hello", "hello", "hel", 28945},
			[]interface{}{"28945", "348777", "28944", nil},
			[]interface{}{"abc", "abc", "abb", 'b'},
		},

		//Less than comparisons
		QueryCompareLessThan: [][]interface{}{
			//layout = {value}, {less value}, {equal or greater value}, {invalid value}
			[]interface{}{int8(5), int8(1), int8(5), uint8(4)},
			[]interface{}{int16(5), int16(1), int16(55), uint8(4)},
			[]interface{}{int32(5), int32(-51), int32(34), "4"},
			[]interface{}{int64(5), int64(1), int64(5), uint8(4)},
			[]interface{}{uint8(5), uint8(1), uint8(15), true},
			[]interface{}{uint16(5), uint16(1), uint16(67), false},
			[]interface{}{uint32(5), uint32(1), uint32(5), nil},
			[]interface{}{uint64(5), uint64(1), uint64(89), "test"},
			[]interface{}{float32(5), float32(1), float32(5), true},
			[]interface{}{float64(5), float64(1), float64(32), uint8(4)},
			[]interface{}{"hello", "hell", "helloooo", 28945},
			[]interface{}{"28945", "28943", "484445", 28945},
			[]interface{}{"abc", "abb", "abd", false},
		},
		//Less than or equal to comparisons
		QueryCompareLessThanEqualTo: [][]interface{}{
			//layout = {value}, {less or equal value}, {greater value}, {invalid value}
			[]interface{}{int8(5), int8(1), int8(7), uint8(4)},
			[]interface{}{int16(5), int16(2), int16(88), uint8(4)},
			[]interface{}{int32(5), int32(4), int32(977), "4"},
			[]interface{}{int64(5), int64(3), int64(46), uint8(4)},
			[]interface{}{uint8(5), uint8(1), uint8(17), true},
			[]interface{}{uint16(5), uint16(4), uint16(81), false},
			[]interface{}{uint32(5), uint32(2), uint32(54), uint8(4)},
			[]interface{}{uint64(5), uint64(4), uint64(771), uint8(4)},
			[]interface{}{float32(5), float32(-1.1), float32(5.1), true},
			[]interface{}{float64(5), float64(-32), float64(866.51), uint8(4)},
			[]interface{}{"hello", "hell", "hellooo", nil},
			[]interface{}{"28945", "1487", "28946", 28945},
			[]interface{}{"abc", "abc", "abd", '#'},
		},
		//Like comparison
		QueryCompareLike: [][]interface{}{
			//layout = {value}, {like}, {not like}, {invalid value}
			[]interface{}{"fox*", "fox jumped", "jumped", 43},
			[]interface{}{"*mped", "fox jumped", "jumped fox", false},
			[]interface{}{"fox*ed", "fox jumped", "jumped", uint8(4)},
			[]interface{}{"fox jumped", "fox jumped", "jumped", 3.43},
			[]interface{}{"fo*ju*", "fox jumped", "jumped", -5},
			[]interface{}{"*5678*", "1234567890", "555555", 'A'},
			[]interface{}{"1*9", "123456789", "000123456789", true},
		},
		//Not Like comparison
		QueryCompareNotLike: [][]interface{}{
			//layout = {value}, {not like}, {like}, {invalid value}
			[]interface{}{"fox*", "jumped", "fox jumped", 43},
			[]interface{}{"*mped", "jumped fox", "fox jumped", false},
			[]interface{}{"fox*ed", "jumped", "fox jumped", uint8(4)},
			[]interface{}{"fox jumped", "fennec jumped", "fox jumped", 3.43},
			[]interface{}{"fo*ju*", "jumped", "fox jumped", -5},
			[]interface{}{"*5678*", "55555", "1234567890", 'A'},
			[]interface{}{"1*9", "0001133339999", "123456789", true},
		},
	}
	qc := &QueryCondition{}
	for comp, matrix := range tests {
		qc.Comparison = comp
		for _, set := range matrix {
			qc.Value = set[0]
			m, err := qc.compare(set[1])
			if err != nil {
				t.Errorf("Error comparing values \"%v\" %v \"%v\". Error: %s", set[0], comp, set[1], err)
			} else if m != true {
				t.Errorf("Error comparing values \"%v\" %v \"%v\", expected True, but received False.", set[0], comp, set[1])
			}
			m, err = qc.compare(set[2])
			if err != nil {
				t.Errorf("Error comparing values \"%v\" %v \"%v\". Error: %s", set[0], comp, set[2], err)
			} else if m == true {
				t.Errorf("Error comparing values \"%v\" %v \"%v\", expected False, but received True.", set[0], comp, set[2])
			}
			m, err = qc.compare(set[3])
			if err == nil {
				t.Errorf("Expected error from mismatched kinds during %q comparison, but nil was returned.", comp)
			}
		}
	}
}
