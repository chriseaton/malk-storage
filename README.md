![malk storage](https://gitlab.com/uploads/project/avatar/1059093/malk-storage.png "malk storage")
# [malk-storage](https://gitlab.com/chriseaton/malk-storage)

This is the storage entity/model engine for [Malk](https://gitlab.com/chriseaton/malk) (content management system).
It's purpose is to provide the interfaces and structures necessary to create a malk storage driver that is
interoperable and interchangeable by malk users, and to define the core functionality for managing models and entities.

**Models**
: Models are concrete structures that define data that can be stored in association with an entity. Models can be
associated with multiple entities, and each model instance represents a "unit" of data that can be created, edited and
destroyed.

**Entities**
: Entities are simply named "representations" of something managed by Malk. Each entity has one (and only one)
associated model. That model not only defines what data can be stored with entity, but also stores each unit of data
associated with a particular entity.

**Storage Driver**
: The storage driver is responsible for storing models in relation to an entity. It provides common "Save", "Get",
"Delete", and "Query" functions that work with a data storage medium (files on a hard disk, or a database engine for
example) to manage model instances.

//TODO Create diagram image

## Database Analogy
Imagine a common database system. A database has an engine, tables, and data.
 - Malk's "Storage Driver" represents the database engine itself, managing data, organizing and responding to operation
   calls for data held within.
 - "Entities" are like tables, they simply represent a set of potential data. In fact, the Malk *sql storage drivers
   often name tables after the entities defined in a Malk setup.
 - "Models" best represent the actual rows within tables. Multiple model instances get stored in association
   with an entity. Models also define the "columns" or structure of an entity. Since a model defines structure, you
   can use the same model definition on multiple entities.