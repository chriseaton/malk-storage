/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

type DriverConfiguration interface{}

// The storage driver is responsible for storing models in relation to an entity. It provides common "Save", "Get",
// "Delete", and "Query" functions that work with a data storage medium (files on a hard disk, or a database engine for
// example) to manage model instances.
type Driver interface {

	// Called by the malk server when configuration occurs.
	// The purpose of this call is to establish and validate the storage medium based on the current configuration
	// of the driver.
	Configure(c DriverConfiguration, r *Registry) error

	// Saves a model to the storage medium. Returns the uint64 identifier assigned by the storage medium.
	Save(entity string, m Model) (uint64, error)

	// Save all specified models to the storage medium. If an error occurs, saving is halted at the model causing the
	// failure.
	SaveAll(entity string, models ...Model) error

	// Loads model data identified by it's uint64 ID value into itself.
	Get(entity string, id uint64) (Model, error)

	// Load all model data for models specified using each one's uint64 ID value. If an error occurs the models found
	// so far will be returned along with the error of the failing Model.
	GetAll(entity string, ids ...uint64) ([]Model, error)

	// Checks if all ids match an existing model.
	// Returns True if all ids are found, False if one or more is missing.
	Exists(entity string, ids ...uint64) (bool, error)

	// Queries the storage engine for models matching the provided query conditions and returns and returns any
	// matching models.
	Query(q *Query) ([]Model, error)

	// Queries the storage engine for models matching the provided query conditions and returns specific field
	// values. Useful for only looking up a sub-set of information within all models.
	QueryFields(q *Query, fields ...string) (QueryFieldResultMap, error)

	// Deletes a model from the storage medium. Returns True if the model was found and removed or False if
	// the model wasn't removed because it already appears deleted (not found).
	// An error is only returned if there is a storage related error deleting the record that isn't due to the record
	// not being found.
	Delete(entity string, id uint64) (bool, error)

	// Delete all the specified models from the storage medium.
	// An error is only returned if there is a storage related error deleting the record that isn't due to the record
	// not being found.
	DeleteAll(entity string, ids ...uint64) error

	// Truncates (deletes) all models associated with the entity.
	Truncate(entity string) error
}
