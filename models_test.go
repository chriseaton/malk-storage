/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

// Tests the isModelFieldType function to ensure that valid field types return True, indicating they are supported.
func TestValidModelFieldTypes(t *testing.T) {
	//create slice of supported values (will resolve to types)
	boolPtr := true
	strPtr := "bogus"
	intPtr := 142342
	int8Ptr := int8(42)
	int16Ptr := int16(3353)
	int32Ptr := int32(458943)
	int64Ptr := int64(90243924025552)
	uintPtr := uint(4442443)
	uint8Ptr := uint8(200)
	uint16Ptr := uint16(4894)
	uint32Ptr := uint32(538839)
	uint64Ptr := uint64(4898389359389348)
	float32Ptr := float32(43453.42421)
	float64Ptr := float64(2098208242.240222555)
	timePtr := time.Now()
	subModelPtr := struct {
		BaseModel
		Title string
		Stars byte
	}{
		Title: "The quick brown fox...",
		Stars: byte(8),
	}
	supportedValues := []interface{}{
		true,
		&boolPtr,
		"hello",
		&strPtr,
		142342,
		&intPtr,
		int8(42),
		&int8Ptr,
		int16(3353),
		&int16Ptr,
		int32(458943),
		&int32Ptr,
		int64(90243924025552),
		&int64Ptr,
		uint(4442443),
		&uintPtr,
		uint8(200),
		byte(144),
		&uint8Ptr,
		uint16(4894),
		&uint16Ptr,
		uint32(538839),
		&uint32Ptr,
		uint64(4898389359389348),
		&uint64Ptr,
		float32(43453.42421),
		&float32Ptr,
		float64(2098208242.240222555),
		&float64Ptr,
		time.Now(),
		&timePtr,
		struct {
			BaseModel
			Name string
		}{
			Name: "This is a legal model field (it's a sub-model!)",
		},
		&subModelPtr,
	}
	//make sure all the values in the slice, as types, are returning as "supported" (True)
	for _, v := range supportedValues {
		vt := reflect.TypeOf(v)
		name := vt.Name()
		if vt.Kind() == reflect.Ptr {
			name = "*" + vt.Elem().Name()
		}
		assert.True(t, isModelFieldType(vt), "Expected type %q to return True (supported type).", name)
	}
}

// Tests the isModelFieldType function to ensure that invalid field types return False, indicating they are not
// supported.
func TestInvalidModelFieldTypes(t *testing.T) {
	//create slice of unsupported values (will resolve to types)
	unsupportedValues := []interface{}{
		complex64(3490234234234233903490433.433340395393990533),
		complex128(90289038903284823892392890348234023.213480123750195129049712304239393),
		uintptr(434344),
		struct {
			This      string
			Is        string
			Not       string
			Supported string
		}{"Sorry", "About", "That", "Pringles"},
	}
	//make sure all the values in the slice, as types, are returning as "unsupported" (False)
	for _, v := range unsupportedValues {
		vt := reflect.TypeOf(v)
		name := vt.Name()
		if vt.Kind() == reflect.Ptr {
			name = "*" + vt.Elem().Name()
		}
		assert.False(t, isModelFieldType(vt), "Expected type %q to return False (supported type).", name)
	}
}
