/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"reflect"
	"time"
)

var (

	// The list of supported interface types that a field can implement to be supported as a Malk model field.
	modelFieldInterfaceTypes = []reflect.Type{reflect.TypeOf((*Model)(nil)).Elem()}

	// The list of struct types that are supported by Malk as Model fields.
	modelFieldStructTypes = []reflect.Type{reflect.TypeOf(time.Time{})}

	// Model field kinds that are supported by Malk.
	modelFieldKinds = []reflect.Kind{reflect.Bool, reflect.String, reflect.Int, reflect.Int8, reflect.Int16,
		reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64}
)

// isTimeType returns True if the given type is a time.Time or *time.Time type, otherwise, False is returned.
func isTimeType(t reflect.Type) bool {
	k := t.Kind()
	timeType := modelFieldStructTypes[0]
	if k == reflect.Ptr || k == reflect.Struct {
		if (k == reflect.Ptr && t.Elem() == timeType) || (k == reflect.Struct && t == timeType) {
			return true
		}
	}
	return false
}

// isModelFieldType will return True if the specified type argument is a type supported by Malk as a model's
// field. False is returned if it is not supported.
func isModelFieldType(t reflect.Type) bool {
	k := t.Kind()
	if k == reflect.Ptr || k == reflect.Struct {
		//check if a valid interface
		for _, mit := range modelFieldInterfaceTypes {
			if reflect.PtrTo(t).Implements(mit) || t.Implements(mit) {
				return true
			}
		}
		//check if a valid struct
		for _, mst := range modelFieldStructTypes {
			if (k == reflect.Ptr && t.Elem() == mst) || (k == reflect.Struct && t == mst) {
				return true
			}
		}
	}
	//check if the kind is directly supported, this is usually just for primitives.
	if k == reflect.Ptr {
		//resolve a pointer and get the pointer element's kind.
		k = t.Elem().Kind()
	}
	for _, v := range modelFieldKinds {
		if v == k {
			return true
		}
	}
	return false
}

// Models are concrete structures that define data that can be stored in association with an entity. Models can be
// associated with multiple entities, and each model instance represents a "unit" of data that can be created, edited
// and destroyed.
type Model interface {

	// Returns the unique identifier (ID) of the model. If the ID is 0, it indicates that the model is unsaved to
	// storage, and will need a new ID generated for it.
	ID() uint64

	// Updates the ID value of the model with the value specified.
	SetID(uint64)
}

// The BaseModel struct can be used as an embedded field on a struct to satisfy the Model interface. It provides the
// ID & SetID funcs that get and set an internal unique ID field.
type BaseModel struct {
	uniqueID uint64
}

// Returns the unique ID value.
func (b *BaseModel) ID() uint64 {
	return b.uniqueID
}

// Sets the unique ID value.
func (b *BaseModel) SetID(id uint64) {
	b.uniqueID = id
}
