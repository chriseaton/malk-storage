/*****************************************************************************************
 * Copyright (c) 2016 Christopher Eaton
 * https://github.com/chriseaton/malk-storage
 * This source code is subject to the terms of the the Universal Permissive License v1.0.
 ****************************************************************************************/
package storage

import (
	"fmt"
	"reflect"
	"regexp"
	"sort"
	"strings"
	"unicode"
)

// Error strings related to entities, models, and the entity/model register.
const (
	ErrorEntityNotRegistered        = "The entity %q has not been registered."
	ErrorEntityModelMismatch        = "The model type %[1]q is not registered with entity %[2]q. Entity %[2]q expects models of type %[3]q."
	ErrorModelNotFound              = "The entity model %q with ID \"%d\" was not found."
	ErrorModelTypeNotSupported      = "The type %q is not supported as a model."
	ErrorModelFieldTypeNotSupported = "The type %q is not supported as an exported model field."
)

// Regex used to scan for invalid runes in an entity name.
// Generally an entity name must simply be alpha-numeric with dashes and underscores allowed.
var invalidEntityNameRunes = regexp.MustCompile("[^A-z0-9-_ ]")

// Map type that stores a reflected field path and the associated kind for a Model.
type ModelMap map[string]ModelField

// SortedKeys returns the keys of the map in a sorted string slice.
func (m ModelMap) SortedKeys() []string {
	sk := make([]string, len(m))
	i := 0
	for k := range m {
		sk[i] = k
		i++
	}
	sort.Strings(sk)
	return sk
}

// Stores the name and type information of a reflected model field.
type ModelField struct {
	Type     reflect.Type
	SubModel bool
	Name     string
}

// Map type that stores reflected field paths and values as well as type information.
type ModelValueMap map[string]ModelFieldValue

// SortedKeys returns the keys of the map in a sorted string slice.
func (m ModelValueMap) SortedKeys() []string {
	sk := make([]string, len(m))
	i := 0
	for k := range m {
		sk[i] = k
		i++
	}
	sort.Strings(sk)
	return sk
}

// Stores the value, name, and type information of a reflected model field.
type ModelFieldValue struct {
	ModelField
	Value interface{}
}

// Returns multiple model value map items using the paths specified. If no paths are specified, all value items
// are returned.
// If a field path is not found an error is returned.
func (m ModelValueMap) Items(paths ...string) ([]ModelFieldValue, error) {
	if len(paths) == 0 {
		r := make([]ModelFieldValue, 0)
		//return all values
		for _, v := range m {
			r = append(r, v)
		}
		return r, nil
	} else {
		r := make([]ModelFieldValue, len(paths))
		//return only named values
		for i, fp := range paths {
			v, ok := m[fp]
			if ok {
				r[i] = v
			} else {
				return nil, fmt.Errorf("Failed to resolve field path %q.", fp)
			}
		}
		return r, nil
	}
}

// Stores a set of model information to facilitate runtime creation and management.
type Registry struct {
	entries map[string]RegistryEntry
}

// Stores reflected data about the model associated with a particular entry.
type RegistryEntry struct {

	// The type of the Model registered for the entity.
	Type reflect.Type

	// Function called to create a new instance of the Model for the entity.
	New func() Model

	// Map of all supported fields (and sub-fields) and the associated type kind of that field value.
	Fields ModelMap
}

// Returns a slice of strings holding all entity names defined in the registry.
func (r *Registry) Entities() []string {
	if r.entries != nil {
		keys := make([]string, len(r.entries))
		i := 0
		for k := range r.entries {
			keys[i] = k
			i++
		}
		return keys
	}
	return nil
}

// Add a model registration entry to the registry. Entity names are case-insensitive and saved in lower-case form.
func (r *Registry) Add(entity string, newFunc func() Model) error {
	//ensure map is created
	if r.entries == nil {
		r.entries = make(map[string]RegistryEntry)
	}
	//check that the entity name only contains valid characters
	if entity != invalidEntityNameRunes.ReplaceAllString(entity, "") {
		return fmt.Errorf("The entity name contains invalid characters, only letters, numbers, dashes, underscores, and spaces are allowed.")
	}
	//check if entity hasn't been added
	if r.Exists(entity) == false {
		if newFunc == nil {
			return fmt.Errorf("The %q function for Model registry entry %q must be defined.", "newFunc", entity)
		}
		m := newFunc()
		if m == nil {
			return fmt.Errorf("The %q function for Model registry entry %q did not return a Model when called.", "newFunc", entity)
		}
		fm := make(ModelMap)
		//use reflection to store type information and build a field map
		mt := reflect.Indirect(reflect.ValueOf(m)).Type()
		err := r.createFieldMap(fm, "", m)
		if err != nil {
			return err
		}
		//create the entry in the registry
		r.entries[strings.ToLower(entity)] = RegistryEntry{
			Type:   mt,
			New:    newFunc,
			Fields: fm,
		}
		return nil
	}
	return fmt.Errorf("A model with the key %q has already been registered.", entity)
}

// Creates a flat field map of the given struct (Model), only mapping supported fields and sub-structs.
// Returns an error if the model type is not supported or an unsupported "exported" field is found.
func (r *Registry) createFieldMap(fm ModelMap, parentPath string, m interface{}) error {
	mv := reflect.Indirect(reflect.ValueOf(m))
	mt := mv.Type()
	//insert specially handled field: ID
	fm[parentPath+"ID"] = ModelField{
		Type: reflect.TypeOf(uint64(0)),
		Name: "ID",
	}
	//add the fields from m
	for i := 0; i < mv.NumField(); i++ {
		fv := mv.Field(i)
		ft := fv.Type()
		fk := fv.Kind()
		mtf := mt.Field(i)
		mtfn := mtf.Name
		exported := ([]rune(mtfn)[0] == unicode.ToUpper([]rune(mtfn)[0]))
		if exported && mtfn != "_" && mtf.Anonymous == false {
			if fv.IsValid() {
				path := parentPath + mtfn
				if isModelFieldType(ft) {
					field := ModelField{
						Type: ft,
						Name: mtfn,
					}
					//handle structs or ptrs to structs, ignore structs that are handled specially (like time.Time).
					if (fk == reflect.Struct || fk == reflect.Ptr) && isTimeType(ft) == false {
						field.SubModel = true
						if fk == reflect.Struct {
							if err := r.createFieldMap(fm, path+".", fv.Interface()); err != nil {
								return err
							}
						} else if fk == reflect.Ptr {
							//check element type of pointer, and if a struct, reflect through it.
							ptrk := ft.Elem().Kind()
							if ptrk == reflect.Struct {
								if err := r.createFieldMap(fm, path+".", reflect.Zero(ft.Elem()).Interface()); err != nil {
									return err
								}
							}
						}
					}
					//add to the map
					fm[path] = field
				} else {
					return fmt.Errorf(ErrorModelFieldTypeNotSupported, ft.Name())
				}
			} else {
				return fmt.Errorf(ErrorModelFieldTypeNotSupported, ft.Name())
			}
		}
	}
	return nil
}

// Valid checks if the given entity name is valid, and that the model specified is registered with said entity.
// Returns nil if there are no issues, or will return an error if the entity or model appears invalid.
func (r *Registry) Valid(entity string, m Model) error {
	entry, err := r.get(entity)
	if err == nil {
		mt := reflect.Indirect(reflect.ValueOf(m)).Type()
		if entry.Type != mt {
			return fmt.Errorf(ErrorEntityModelMismatch, mt.Name(), entity, entry.Type.Name())
		}
		return nil
	}
	return err
}

// Checks if a registry entry for the given entity exists. Entity names are case-insensitive.
func (r *Registry) Exists(entity string) bool {
	if r.entries != nil {
		_, ok := r.entries[strings.ToLower(entity)]
		return ok
	}
	return false
}

// Clears out all registry entries.
func (r *Registry) Clear() {
	r.entries = nil
}

// Removes a registry entry for the named entity. Entity names are case-insensitive.
func (r *Registry) Remove(entity string) error {
	if r.Exists(entity) {
		delete(r.entries, strings.ToLower(entity))
		return nil
	}
	return fmt.Errorf(ErrorEntityNotRegistered, entity)
}

// Creates a new Model for the specified entity. Returns nil if the entity name specified is invalid or the New function
// is not defined.
func (r *Registry) New(entity string) Model {
	entity = strings.ToLower(entity)
	if r.Exists(entity) && r.entries[entity].New != nil {
		return r.entries[entity].New()
	}
	return nil
}

// Returns the reflect.Type of the Model associated with the specified entity.
func (r *Registry) ModelTypeOf(entity string) (*reflect.Type, error) {
	entry, err := r.get(entity)
	if err == nil {
		return &entry.Type, nil
	}
	return nil, err
}

// Returns the field map of the model associated with the specified entity.
func (r *Registry) FieldMap(entity string) (ModelMap, error) {
	entry, err := r.get(entity)
	if err == nil {
		return entry.Fields, nil
	}
	return nil, err
}

// Creates a value map for the entity's associated model, using the data stored in the given model instance.
//
// The map will only populate field paths, if the path points to a struct, it will leave the struct value as nil. This
// is due to the fact that fields in that struct should already be mapped, so there's no need to duplicate data.
// If you want to get the struct within a model, you can call the GetValue function.
//
// An error is returned if the provided model does not match the entity's model type (strict type enforcement).
func (r *Registry) FieldValueMap(entity string, m Model) (ModelValueMap, error) {
	entry, err := r.get(entity)
	if err == nil {
		mv := reflect.Indirect(reflect.ValueOf(m))
		mt := mv.Type()
		if mt != entry.Type {
			return nil, fmt.Errorf("The model type %q given doesn't match the entity's registered model type %q.", mt.Name(), entry.Type.Name())
		}
		fvm := make(ModelValueMap)
		for path, f := range entry.Fields {
			v, err := r.GetValue(entity, m, path)
			if err != nil {
				continue
			}
			fvm[path] = ModelFieldValue{
				ModelField: ModelField{
					Type:     f.Type,
					SubModel: f.SubModel,
					Name:     f.Name,
				},
				Value: v,
			}
		}
		return fvm, err
	}
	return nil, err
}

// Returns the value of the field of a given path on the specified model.
func (r *Registry) GetValue(entity string, m Model, fieldPath string) (interface{}, error) {
	entry, err := r.get(entity)
	if err == nil {
		fv := reflect.Indirect(reflect.ValueOf(m))
		ft := fv.Type()
		fk := ft.Kind()
		if ft != entry.Type {
			return nil, fmt.Errorf("The model type %q given doesn't match the entity's registered model type %q.", ft.Name(), entry.Type.Name())
		} else if fieldPath == "ID" {
			return m.ID(), nil
		} else {
			//get path segments
			path := strings.Split(fieldPath, ".")
			//loop through each segment
			for i, p := range path {
				if p == "ID" { //handle Model IDs (special case)
					//last set fv should be a Model
					fvm, ok := fv.Interface().(Model)
					if ok {
						return fvm.ID(), nil
					} else {
						return nil, fmt.Errorf("Unable to convert value of %q to Model.", fv.Type().Name())
					}
				} else {
					if fk == reflect.Ptr {
						fv = fv.Elem()
					}
					fv = fv.FieldByName(p)
					ft = fv.Type()
					fk = ft.Kind()
					if fk == reflect.Ptr {
						if fv.IsNil() {
							//check if we're on the last path item
							if i == len(path)-1 {
								return nil, nil
							} else {
								return nil, fmt.Errorf("Failed to get value using path %q on entity %q model with ID %d. The path encountered a nil value on a path mid-point %q.", fieldPath, entity, m.ID(), p)
							}
						}
					}
				}
			}
		}
		if fv.IsValid() {
			return fv.Interface(), nil
		} else {
			return nil, fmt.Errorf("Failed to get value using path %q on entity %q model with ID %d.", fieldPath, entity, m.ID())
		}
	}
	return nil, err
}

// Returns the registry entry for the specified entity.
func (r *Registry) get(entity string) (*RegistryEntry, error) {
	if r.entries != nil {
		re, ok := r.entries[strings.ToLower(entity)]
		if ok {
			return &re, nil
		}
	}
	return nil, fmt.Errorf(ErrorEntityNotRegistered, entity)
}
